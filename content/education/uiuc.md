+++
title = "University of Illinois at Urbana-Champaign"

[extra]
image = "https://upload.wikimedia.org/wikipedia/commons/a/a6/University_of_Illinois_at_Urbana-Champaign_Wordmark.svg"
link = "https://illinois.edu/"
+++