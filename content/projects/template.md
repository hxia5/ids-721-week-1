+++
title = "IDS 721 Week 1 Mini Project"

[extra]
image = "https://upload.wikimedia.org/wikipedia/commons/3/35/GitLab_icon.svg"
link = "https://gitlab.com/hxia5/ids-721-week-1"
technologies = ["zola", "Rust", "CSS"]
+++
