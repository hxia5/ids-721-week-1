# IDS 721 Week 1 
[![pipeline status](https://gitlab.com/hxia5/ids-721-week-1/badges/main/pipeline.svg)](https://gitlab.com/hxia5/ids-721-week-1/-/commits/main)

## Link to Website
https://ids-721-week-1-hxia5-988a46c150e456c844de17cb87007485d4f8b1a098.gitlab.io

## Screenshot of Home Page

![Alt text](home.png)

## Screenshot of Portfolio Page

![Alt text](portfolio.png)

## Overview
* This is my repository ofIDS 721 Mini-Project 1 - Create a Static Website with Zola.

## Purpose
- Static site generated with Zola
- Home page and portfolio project page templates
- Styled with CSS
- GitLab repo with source code


## Key Steps
1. `git init` to create a new repository locally
2. Install Zola using `brew install zola`
3. `zola init my_site` to create a new Zola site
4. Choose a template of the theme from the Zola theme, and deploy it by using `cd themes` and then `git clone <theme repository URL>`. I chose the Hephaestus theme. 
5. After this, follow the instructions to customize your web pages. To fulfill the requirements, I added two sections: "Education" and "Projects". In the "Education" section, I added my undergraduate and graduate university with their badges and web links. In the "Projects" section, I added the links to my Gitlab repositories for the mini projects in IDS 721.
6. Create a `.gitmodules` file in the root directory of the repository with the following content:
```
[submodule "themes/hephaestus"]
	path = themes/hephaestus
	url = https://github.com/BConquest/hephaestus.git
```
7. Set up the GitLab CI/CD pipeline to build the site and deploy it to GitLab pages:
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
8. Create a repository on GitLab, pull first and then push the local repository to the remote repository.
```
git pull origin main
git add .
git commit -m "Initial commit"
git push origin main
```
9. On the left sidebar of GitLab, navigate to **Deploy -> Pages** to find the link to the site.

10. On the left sidebar of GitLab, navigate to **settings -> cicd -> general pipeline expand -> pipeline status** to find the passed badge.

References:

https://www.getzola.org/documentation/getting-started/installation/

